import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import axios from "axios";

import crypt from "cryptr";
const cryptr = new crypt('myTotallySecretKey');

class SignInForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            isLoggedIn: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({
            [name]: value
        });
    };

    handleSubmit = e => {
        e.preventDefault();

        console.log("The form was submitted with the following data:");
        console.log([this.state.email, cryptr.encrypt(this.state.password)]);

        axios.post("/login",{
                email: this.state.email,
                password: this.state.password,
            })
        .then(res => {
            this.setState({isLoggedIn: true});
            this.props.history.push('/home');
        })
        .catch(err => {
            console.log(err);
        });
    };

    render() {
        return (
            <div className="FormCenter">
                <form className="FormFields" onSubmit={this.handleSubmit}>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="email">E-Mail Address</label>
                        <input type="email" id="email" className="FormField__Input" placeholder="Enter your email" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>

                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Password</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
                    </div>

                    <div className="FormField">
                        <button className="FormField__Button mr-20">Sign In</button> <Link to="/register" className="FormField__Link">Create an account</Link>
                    </div>
                    {/*<Router basename="/">*/}
                    {/*{this.state.isLoggedIn*/}
                    {/*?<Route exact path="/home" component={Home}></Route>*/}
                    {/*: null}*/}
                    {/*</Router>*/}
                </form>
            </div>
        );
    }
}

export default SignInForm;