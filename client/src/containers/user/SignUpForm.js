import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

import FormErrors from "./FormErrors";

import crypt from "cryptr";
const cryptr = new crypt('myTotallySecretKey');

class SignUpForm extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            username: '',
            hasAgreed: false,
            formErrors: {email: '', password: '', hasAgreed: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        let target = e.target;
        let value = target.type === 'checkbox' ? target.checked : target.value;
        let name = target.name;

        this.setState({[name]: value},
            () => {
                this.validateField(name, value)
            });
    };

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let hasAgreedValid = this.state.hasAgreed;

        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                break;
            case 'password':
                passwordValid = value.match(/^(?=.*[a-zA-Z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i);
                fieldValidationErrors.password = passwordValid ? '': ' is too short, min length is 8 OR must include at least one special character OR must include at least one capital letter OR must include at least number';
                break;
            case 'hasAgreed':
                // hasAgreedValid = hasAgreed;
                if(hasAgreedValid) {
                    fieldValidationErrors.hasAgreedValid = "";
                }
                else {
                    fieldValidationErrors.hasAgreedValid = "You should agree to the terms and conditions";
                }
                break;
            default:
                break;
        }
        this.setState({formErrors: fieldValidationErrors,
            emailValid: emailValid,
            passwordValid: passwordValid,
            hasAgreedValid: hasAgreedValid,
        }, this.validateForm);
    }

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid && this.state.hasAgreedValid});
    }

    handleSubmit = e => {
        e.preventDefault();

        console.log('The form was submitted with the following data:');

        console.log([this.state.email, cryptr.encrypt(this.state.password), this.state.username, this.state.hasAgreed]);

        axios.post("/register",{
                email: this.state.email,
                password:this.state.password,
                username: this.state.username,
                hasAgreed: this.state.hasAgreed
            })
        .then(res => {
            console.log(res);
        })
        .catch(err => {
            console.log(err);
        });
    };

    render() {
        return (
            <div className="FormCenter">
                <form onSubmit={this.handleSubmit} className="FormFields" method="POST">
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="name">Username</label>
                        <input type="text" id="name" className="FormField__Input" placeholder="Enter your username" name="username" value={this.state.username} onChange={this.handleChange} />
                    </div>
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="password">Password</label>
                        <input type="password" id="password" className="FormField__Input" placeholder="Enter your password" name="password" value={this.state.password} onChange={this.handleChange} />
                    <div className="FormField">
                        <label className="FormField__Label" htmlFor="email">E-Mail Address</label>
                        <input type="email" id="email" className="FormField__Input" placeholder="Enter your email" name="email" value={this.state.email} onChange={this.handleChange} />
                    </div>
                    </div>

                    <div className="FormField">
                        <label className="FormField__CheckboxLabel">
                            <input className="FormField__Checkbox" type="checkbox" name="hasAgreed" value={this.state.hasAgreed} onChange={this.handleChange} /> I agree all statements in <a href="/" className="FormField__TermsLink">terms of service</a>
                        </label>
                    </div>

                    <div className="FormField">
                        <button disabled={!this.state.formValid} className="btn FormField__Button mr-20">Sign Up</button> <Link to="/login" className="FormField__Link">I'm already member</Link>
                    </div>
                </form>
                <div className="panel panel-default">
                    <FormErrors formErrors={this.state.formErrors} />
                </div>
            </div>
        );
    }
}

export default SignUpForm;