import express from "express";
import bodyParser from "body-parser";
import fs from "fs";
import crypt from "cryptr";
import redirect from "express-redirect";

const app = express();
const port = process.env.PORT || 5000;
const cryptr = new crypt('myTotallySecretKey');
redirect(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.send({express: "hi"});
});

app.get("/home", (req, res) => {
    return res.redirect("/#/home");
});

app.post("/register",(req, res) => {
    console.log(req.body);
    const email = req.body.email;
    const password = cryptr.encrypt(req.body.password);
    const username = req.body.username;

    let bodyMsg = JSON.stringify({email, password, username});//.replace(/\{|\}|\:|\"/g, "");

    fs.writeFile("../user.txt", `${bodyMsg}`, (err) => {//appendFile
        if (err) throw err;
        console.log('It\'s saved one directory back');
    });
});

app.post("/login",(req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    fs.readFile("../user.txt", "utf8", (err, data) => {
        if(err) throw err;
        console.log(data, "data");
        const dataEmail = JSON.parse(data).email;
        const dataPassword = cryptr.decrypt(JSON.parse(data).password);
        console.log(dataEmail, dataPassword, email, password, "blah");
        if(dataEmail === email && dataPassword === password) {
            console.log("ok");

            return res.redirect("/#/home");
        }
    });
});

app.listen(port, () => {
    console.log(`is listening on port ${port}`);
});